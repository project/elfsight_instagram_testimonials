<?php

namespace Drupal\elfsight_instagram_testimonials\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * {@inheritdoc}
 */
class ElfsightInstagramTestimonialsController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    $url = 'https://apps.elfsight.com/embed/instagram-testimonials/?utm_source=portals&utm_medium=drupal&utm_campaign=instagram-testimonials&utm_content=sign-up';

    require_once __DIR__ . '/embed.php';

    return [
      'response' => 1,
    ];
  }

}
